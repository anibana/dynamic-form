import React from 'react';
import {Field} from 'redux-form';

const AddressFields = () => (
  <div>
    <Field name='address' component='input' />
    <Field name='city' component='input' />
    <Field name='state' component='input' />
    <Field name='zip' component='input' />
  </div>
);

export default AddressFields;

