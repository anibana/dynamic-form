import React, {PropTypes} from 'react';

const SelectField = ({input, options, placeholder, onSelectChange}) => {
  const {onChange, ...rest} = input;

  return (
    <select {...rest} onChange={(e) => {
        onSelectChange && onSelectChange(e.target.value);
        onChange(e);
      }} >
      <option value=''>{placeholder}</option>
      { options.map(option => <option key={option.value} value={option.value}>{option.name}</option>) }
    </select>
  );
};

SelectField.defaultProps = {
  placeholder: 'Please select',
}

SelectField.propTypes = {
  options: PropTypes.arrayOf(
    PropTypes.shape({
      value: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
    })
  ),
  placeholder: PropTypes.string,
  onSelectChange: PropTypes.func,
}

export default SelectField;

