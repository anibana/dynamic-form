import React from 'react';
import {Field} from 'redux-form';

const ConnectingFlightFields = () => (
  <div>
    <Field name='flightNumber' component='input' />
    <Field name='departureCity' component='input' />
  </div>
);

export default ConnectingFlightFields;

