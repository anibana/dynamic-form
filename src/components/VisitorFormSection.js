import React, {Component} from 'react';
import {Field, FormSection} from 'redux-form';

import SelectField from './SelectField';
import AddressFields from './AddressFields';
import ConnectingFlightFields from './ConnectingFlightFields';

class VistorFormSection extends Component {
  constructor(props) {
    super(props);

    this.state = {
      type: ''
    }
  }

  handleSelectChange = (type) => this.setState({type})

  renderSection = () => {
    const {type} = this.state;

    if(!type) {
      return <div />;
    }

    if (type === 'connect') {
      return <ConnectingFlightFields />;
    } else {
      return <AddressFields />;
    }
  }

  render() {
    const options = [
      { value: 'stay', name: 'Staying in the USA' },
      { value: 'cruise', name: 'Joining a cruise' },
      { value: 'rent', name: 'Renting a car' },
      { value: 'connect', name: 'Connecting to an internation flight' }
    ];

    return (
      <FormSection name='visitor'>
        <Field name='type' component={SelectField} onSelectChange={this.handleSelectChange} options={options} />

        { this.renderSection() }
      </FormSection>
    );
  }
}

export default VistorFormSection;

