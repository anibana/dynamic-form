import React, {Component} from 'react';
import {Field, FormSection} from 'redux-form';

import DateFormFields from './DateFormFields';
import SelectField from './SelectField';

class ResidencyForm extends Component {
  render() {
    const options = [
      { value: 'RAC', name: 'Resident Alien Card' },
      { value: 'PRC', name: 'Permanent Resident Card' },
    ];

    return (
      <FormSection name='residency'>
        <Field name='type' component={SelectField} options={options} />

        <Field name='cardNumber' component='input' />

        <FormSection name='expiration'>
          <DateFormFields />
        </FormSection>
      </FormSection>
    );
  }
}

export default ResidencyForm;

