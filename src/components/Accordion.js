import React, {Component, PropTypes} from 'react';

class Accordion extends Component {
  componentWillUpdate(nextProps) {
    if (this.props.expanded !== nextProps.expanded && nextProps.expanded) {
      console.log('do some css transition here, focusing on the opening accordion');
    }
  }

  render() {
    const {header, children, expanded} = this.props;
    const displayedStyle = expanded ? 'block' : 'none';

    return (
      <div>
        {header}
        <div style={{display: displayedStyle}}>
          {children}
        </div>
      </div>
    );
  }
}

Accordion.propTypes = {
  expanded: PropTypes.bool,
  header: PropTypes.any.isRequired,
}

export default Accordion;

