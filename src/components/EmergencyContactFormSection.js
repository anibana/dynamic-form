import React, {Component} from 'react';
import {Field, FormSection} from 'redux-form';

import SelectField from './SelectField';

class EmergencyContactFormSection extends Component {
  constructor(props) {
    super(props);

    this.state = {
      show: false,
    }
  }

  handleSelectChange = (hasContact) => this.setState({show: hasContact === 'yes'})

  renderSection = () => {
    if (this.state.show) {
      return (
        <div>
          <Field name='name' component='input' />
          <Field name='country' component='input' />
          <Field name='phoneNumber' component='input' />
        </div>
      );
    }
  }

  render() {
    const options = [
      { value: 'yes', name: 'Add new emergency contact information' },
      { value: 'no', name: 'I do not want to provide emergency contact information' }
    ];
    return (
      <FormSection name='emergency'>
        <Field name='hasContact' component={SelectField} onSelectChange={this.handleSelectChange} options={options} />

        { this.renderSection() }

      </FormSection>
    );
  }
}

export default EmergencyContactFormSection;

