import React, {Component, PropTypes} from 'react';
import {Field, reduxForm} from 'redux-form';
import _ from 'lodash';

import Accordion from './Accordion';
import SelectField from './SelectField';

import EmergencyContactFormSection from './EmergencyContactFormSection';
import ResidencyFormSection from './ResidencyFormSection';
import VisitorFormSection from './VisitorFormSection';

export class PassengerInfoForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      country: '',
      expanded: true,
    }
  }

  handleSelectChange = (country) => this.setState({country});

  handleHeaderClick = () => this.props.collapsible && this.setState({expanded: !this.state.expanded});

  renderSection = (nationality) => {
    const {country} = this.state;

    if (!country) {
      return <div />;
    }

    if (nationality === 'US') {
      return <EmergencyContactFormSection />;
    }

    if (country === 'US') {
      return <ResidencyFormSection />;
    } else  {
      return <VisitorFormSection />;
    }
  }

  render() {
    const {id, name, nationality} = this.props.customer;
    const {expanded} = this.state;
    const {
      collapsible,
      countries,
      handleSubmit,
      submitSucceeded,
      submitFailed
    } = this.props;

    const header = (
      <header onClick={this.handleHeaderClick}>
        <h1>{name} - {id}</h1>
        <h2>Succeeded: {submitSucceeded ? 'yes' : 'no'}</h2>
        <h2>Failed: {submitFailed ? 'yes' : 'no'}</h2>
      </header>
    );

    return (
      <Accordion header={header} expanded={expanded}>
        <form onSubmit={handleSubmit}>
          <Field name='countryOfResidence' component={SelectField} onSelectChange={this.handleSelectChange} options={countries} />

          { this.renderSection(nationality) }

          { collapsible && <button type='submit' onClick={() => this.setState({expanded: false})}>OK</button> }
        </form>
      </Accordion>
    );
  }
}

PassengerInfoForm.propTypes = {
  customer: PropTypes.shape({
    name: PropTypes.string.isRequired,
    nationality: PropTypes.string.isRequired,
  }).isRequired,
  collapsible: PropTypes.bool,
}

export default reduxForm({
  onSubmit: (values, dispatch, props) => {
    const filteredValues = _.pick(values, props.registeredFields.map(f => f.name));
    console.log(JSON.stringify(filteredValues));
  },
})(PassengerInfoForm);

