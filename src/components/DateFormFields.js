import React from 'react';
import {Field} from 'redux-form';

import SelectField from './SelectField';

// TODO: Refactor this sh*t.
const dateOptions = [
  { value: '01', name: '01' },
  { value: '02', name: '02' },
];

const monthOptions = [
  { value: '01', name: 'January' },
  { value: '02', name: 'February' },
];

const yearOptions = [
  { value: '1999', name: '1999' },
  { value: '2000', name: '2000' },
];

const DateFormFields = () => (
  <div>
    <Field name='date' component={SelectField} options={dateOptions} placeholder='DD' />
    <Field name='month' component={SelectField} options={monthOptions} placeholder='MM' />
    <Field name='year' component={SelectField} options={yearOptions} placeholder='YYYY' />
  </div>
);

export default DateFormFields;

