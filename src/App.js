import React from 'react';
import {Fragment} from 'redux-little-router';

import USFlow from './containers/USFlow';

const App = () => (
  <Fragment forRoute='/'>
    <USFlow />
  </Fragment>
);

export default App;
