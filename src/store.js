import { combineReducers, createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { routerForBrowser } from 'redux-little-router';

import reducers from './reducers';
import routes from './routes.json';

const { reducer, middleware, enhancer } = routerForBrowser({ routes });

const composedMiddleware = [
  applyMiddleware(thunk, middleware)
];

if (process.env.NODE_ENV !== 'production') {
  window.__REDUX_DEVTOOLS_EXTENSION__
  && composedMiddleware.push(window.__REDUX_DEVTOOLS_EXTENSION__());
}

export default function configureStore(initialState) {
  return createStore(
    combineReducers({ router: reducer, ...reducers }),
    initialState,
    compose(enhancer, ...composedMiddleware)
  )
}
