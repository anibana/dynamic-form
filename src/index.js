import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { initializeCurrentLocation, RouterProvider } from 'redux-little-router';
import registerServiceWorker from './registerServiceWorker';
import App from './App';
import configureStore from './store.js';
import './index.css';

const store = configureStore();
const initialLocation = store.getState().router;

if (initialLocation) {
  store.dispatch(initializeCurrentLocation(initialLocation));
}

ReactDOM.render(
  <RouterProvider store={ store }>
    <Provider store={ store }>
      <App />
    </Provider>
  </RouterProvider>,
  document.getElementById('root')
);

registerServiceWorker();
