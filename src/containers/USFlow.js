import React from 'react';
import {connect} from 'react-redux';

import PassengerInfoForm from '../components/PassengerInfoForm';

const USFlow = ({flightCode, customers}) => {
  const collapsible = customers.length > 1;

  const forms = customers.map(customer =>
    <PassengerInfoForm
      collapsible={collapsible}
      form={customer.id}
      key={customer.id}
      customer={customer}
      countries={countries}
    />
  );

  return (
    <div>
      <h1>Flight: going to {flightCode}</h1>

      { forms }
    </div>
  );
}

const customers = [
  {
    id: '123',
    name: 'US citizen',
    nationality: 'US',
  }, {
    id: '456',
    name: 'US Resident PRC/RAC',
    nationality: 'AUS',
  }, {
    id: '789',
    name: 'Tourist',
    nationality: 'FRA',
  }
];

const countries = [
  { value: 'US', name: 'United States' },
  { value: 'AUS', name: 'Australia' },
  { value: 'PH', name: 'Philippines' },
];

function mapStateToProps({form}) {
  return {
    flightCode: 'US',
    customers,
    countries,
  }
}

export default connect(mapStateToProps)(USFlow);
