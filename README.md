## DishSpot

Revolutionary way of hunting. Find best dishes close to you!

Setting up your local machine
* Install dependencies
```
  yarn install
```

* Start server
```
  yarn start
```
